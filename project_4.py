################################################################################
#
# Filename: fsm_and_regex_test.py
# Author: Luke Kanuchok and Chandler Van Dyke==> (functions: re_union(), re_concat(), re_star,
#                                                 cleanupStr(trans_string, fsm), and generate_re_from_fsm(fsm)
# Date: 12/02/2016
#
# Course: CST215
# Project: FSM to RE translator
#
# Description: Example code file exploring the concepts of a FSM to RE
#              translator.
#
# A FSM to RE translator will take the definition of a finite state machine, and
# create a regular expression that matches/accepts the same language. The
# definition of a finite state machine is comprised of the following components:
#  - A set of state names (set S)
#  - An alphabet of input characters (denoted Sigma)
#  - A set of accepting state names (set S_a, a subset of set S)
#  - A designation/name of the initial state (state s_0)
#  - A next-state function, which maps the transitions from state x to state y
#    given input character z.
# Using this data, a finite state machine starts at the starting state s_0, and
# parses through a set of input data (an input string of characters taken from
# alphabet Sigma) until the string has been exhausted. If the current state of
# the finite state machine is an accepting state (as determined by the input
# set of accepting states, S_a) then the input string is a part of the language.
# If the machine is not in an accepting state, the input string is not part of
# the language.
#
# Similarly, a regular expression is used to match an input string by
# successively matching one or more characters against a pattern string. If the
# input string matches the pattern string, the input string is accepted. If the
# input string does not match the pattern string, the input string is not part
# of the language defined by the regular expression, and is rejected.
#
# A regular expression uses grouping (by way of parentheses),
# concatenation (adjacent characters), alternation (indicated by "|"), and
# repetition (indicated by "*" for zero or more times).
#
# Examples:
# - Grouping      (from "abc", we can get "(ab)c")
# - Concatenation (from "a" and "b", we can get "ab")
# - Alternation   (from "a" and "b", we can get "a|b")
# - Repetition    (from "a", we can get "a*"
#
################################################################################

default_data = (
    #  - A set of state names (set S)
    ('s0', 's1', 's2'),
    #  - An alphabet of input characters (denoted Sigma)
    ('a','b'),
    #  - A set of accepting state names (set S_a, a subset of set S)
    ('s2',),
    #  - A designation/name of the initial state (state s_0)
    's0',
    #  - A next-state function, which maps the transitions from state x to
    #    state y given input character z.
    #    ((state-originating-transition,
    #      input character,
    #      destination-state),
    #     (...),
    #    )
    (('s0','a','s1'),
     ('s0','b','s2'),
     ('s1','a','s0'),
     ('s1','b','s0'),
     ('s2','a','s0'),
     ('s2','b','s0'),),
    )


def get_xn_chars(transition_list, orig_state, dest_state):
    # Take the orig_state and dest_state given, and walk through the 
    # transition_list. If a transition can take fsm from orig to dest, add
    # the transition char to the result set. Return a sorted tuple of the
    # characters, or None if no transitions are found.
    result = set()
    for orig, xn_char, dest in transition_list:
        if orig_state == orig and dest_state == dest:
            result.add(xn_char)
    return tuple(sorted(result)) if len(result) > 0 else None


def next_state(transition_list, state_name, input_char):
    # Take the state name given, and find the transition in the transition
    # table that matches the input_char given, return the final value in
    # that tuple (the name of the destination state), or None of not found.
    for orig_state, xn_char, dest_state in transition_list:
        if orig_state == state_name and input_char == xn_char:
            return dest_state
    return None  # If not found


def fsm_match_str(fsm_data, input_string):
    # Use the FSM (provided using fsm_data) to determine if the input_string
    # is accepted.
    # fsm_data has the following order:
    # elem 0: Set of state names (tuple of strings)
    # elem 1: Alphabet of allowed input characters (tuple of length1 strings)
    # elem 2: Set of accepting state names (tuple of strings)
    # elem 3: Name of initial state (string)
    # elem 4: Next-state function (tuple of 3-tuples:
    #                                (string (name of originating state),
    #                                 string of length 1 (transition char),
    #                                 string (name of destination state)
    #                             )
    st_cur = fsm_data[3]  # Initial state
    for c in input_string:
        st_next = next_state(fsm_data[4], st_cur, c)
        if st_next == None:
            # Illegal transition!
            msg = "Illegal transition:"
            msg += "(State '{}' has no transition with character '{}'".format(
                   st_cur, c)
            return False, msg
        st_cur = st_next
    if st_cur in fsm_data[2]:  # final state in list of accepting states
        return True, "fsm matches input string"
    else:
        return False, "fsm left in non-accepting state at end of input"


################################################################################
# Helper functions
#
# load_all_data_from_file(filename)
#  - Used to create and test FSMs
#  - Open filename, read and process data, and return results.
#
# re_union(re_list)
#  - Return the union of all elements in re_list.
# 
# re_concat(re1, re2)
#  - Return the concatenation of regex 1 (re1) and regex2 (re2).
#
# re_star(re)
#  - Return the Kleene closure of re as a regular expression.
#
# generate_re_from_fsm(fsm)
#  - Using the fsm, calculate a regular expression that matches the fsm and
#    return it.
#
################################################################################

def load_all_data_from_file(filename):
    # Load a set of data from a file.
    # The necessary data is: a set of state names, an alphabet (list of
    # valid input values, limited to single characters for this
    # implementation), a set of accepting state names, an initial state
    # (name) and a series of transitions (also known as a next-state
    # function).
    # Data format (per file):
    # A line containing an integer (x, the number of FSM/dataset pairs)
    #
    # Per FSM:
    # A line containing a comma delimited set of strings (state names)
    # A line containing a comma delimited set of characters (alphabet)
    # A line containing a comma delimited set of strings (accepting states)
    # A line containing a string (initial state)
    # A line containing an integer (n; number of transitions, one per line)
    # n lines containing 3 space delimited values (state, char, state)
    # where the states are in the state names, and char is in the alphabet
    #
    # Per dataset group:
    # A line containing two integers (space seperated)
    # - p, the number of lines that contain strings that are acceptable
    # - q, the number of lines that contain strings that are unacceptable
    # p lines containing strings for verifying the FSM (acceptable)
    # q lines containing strings for verifying the FSM (unacceptable)
    #
    # Note: All whitespace will be preserved as significant in the datafile,
    #       with exception of stripping the newline from the end of strings.
    fsm_and_tests = []
    with open(filename, 'r') as in_f:
        num_tests = int(in_f.readline())
        for n in range(num_tests):
            state_name_line = in_f.readline()[:-1]
            alphabet_line   = in_f.readline()[:-1]
            accept_line     = in_f.readline()[:-1]
            initial_line    = in_f.readline()[:-1]
            n_str           = in_f.readline()[:-1]
            n = int(n_str)
            dataset_lines = [in_f.readline()[:-1] for i in range(n)]

            state_names = tuple(x.strip() for x in state_name_line.split(","))
            alphabet    = tuple(x for x in alphabet_line.split(","))
            accepting_states = tuple(x for x in accept_line.split(","))
            initial_state = initial_line.strip()
            transitions = tuple(tuple(x for x in ds.strip().split(" "))
                                for ds in dataset_lines)
            fsm = (state_names,
                   alphabet,
                   accepting_states,
                   initial_state,
                   transitions)
            
            p, q = [int(x) for x in in_f.readline().strip().split()]
            accept_test_cases = [in_f.readline()[:-1] for i in range(p)]
            unaccept_test_cases = [in_f.readline()[:-1] for i in range(q)]
            
            fsm_and_tests.append((fsm, accept_test_cases, unaccept_test_cases))
    return fsm_and_tests

###############################################################################
###############################################################################
#FIXme
def re_union(re_list):
    # Return the union of all elements in re_list.
    result = ""
    re_list = [x for x in re_list if x != None]
    if re_list == []: return None
    if len(set(re_list)) == 1: re_list = list(set(re_list))
    result = "|".join(re_list)
    return result


def re_concat(re1, re2):
    # Return the concatenation of regex 1 (re1) and regex2 (re2)
    result = ""
    if None in (re1, re2): return None
    result = re1 + re2
    return result


def re_star(re):
    # Return the Kleene closure of re as a regular expression
    result = ""
    if re == None or re == "": return ""
    result = re+"*"
    return result

def cleanupStr(trans_string, fsm):
    #return a cleaned up R state
    cleanup = 0
    while cleanup == 0:
        orig = trans_string
        for letter in fsm[1]:
            trans_string = trans_string.replace("("+letter+")", letter)
            trans_string = trans_string.replace("( "+letter+")", letter)
            trans_string = trans_string.replace("("+letter+" )", letter)
            trans_string = trans_string.replace(letter+"|"+letter, letter)
            trans_string = trans_string.replace(letter+letter+"*", letter+"*")
            trans_string = trans_string.replace("("+letter+letter+")",
                                                letter+letter)
        trans_string = trans_string.replace("( )", "")

        trans_string = trans_string.replace(" *", "")

        trans_string = trans_string.replace(" |", "")
        trans_string = trans_string.replace("| ", "")
        trans_string = trans_string.replace("(|", "(")
        trans_string = trans_string.replace("|)", ")")
        trans_string = trans_string.replace("()", " ")

        if trans_string == orig:
            cleanup +=1

    return trans_string

#FIXme
def generate_re_from_fsm(fsm):
    # Use the FSM (provided using fsm_data) to calculate a regular expression
    # and return it.
    # fsm has the following order:
    # elem 0: Set of state names (tuple of strings)
    # elem 1: Alphabet of allowed input characters (tuple of length1 strings)
    # elem 2: Set of accepting state names (tuple of strings)
    # elem 3: Name of initial state (string)
    # elem 4: Next-state function (tuple of 3-tuples:
    #                                (string (name of originating state),
    #                                 string of length 1 (transition char),
    #                                 string (name of destination state)
    #                              )
    #get the R(-1, x, x) values
    kleens_dict = {}
    for state in fsm[0]:
        for second_state in fsm[0]:
            trans_lst=[]
            for initial, char, dest in fsm[4]:
                if initial == state and dest == second_state:
                    trans_lst.append(char)
            if state == second_state:
                trans_lst.append(" ")
            if len(trans_lst) == 0:
                trans_lst.append("null")
            kleens_dict["(-1, {}, {})".format(state, second_state)]  = \
                "("+"|".join(trans_lst)+")"

    #get the rest of the R values
    iterations = len(fsm[0])
    for iteration in range(0,iterations):
        for state in fsm[0]:
            for second_state in fsm[0]:
                #kleen's algorithm
                k = iteration
                prev_k = k - 1
                i = state
                j = second_state
                trans_string = "("
                for key, value in kleens_dict.items():
                    if key == "({}, {}, s{})".format(prev_k, i, k):
                        trans_string += "("+value+")"
                for key, value in kleens_dict.items():
                    if key == "({}, s{}, s{})".format(prev_k, k, k):
                        trans_string += "("+value+")*"
                for key, value in kleens_dict.items():
                    if key == "({}, s{}, {})".format(prev_k, k, j):
                        trans_string += "("+value+")"
                if trans_string.find("null") != -1:
                    trans_string = "(null"
                trans_string += ")"
                for key, value in kleens_dict.items():
                    if key == "({}, {}, {})".format(prev_k, i, j):
                        if value.find("null") != -1:
                            value = ""
                        trans_string += "|"+value

                trans_string = cleanupStr(trans_string, fsm)
                kleens_dict["({}, {}, {})".format(iteration, state,
                                                  second_state)] = trans_string
    #bring all accepting states together
    regex_lst = []
    for state in fsm[2]:
        regex_lst.append(kleens_dict["({}, {}, {})".format(iterations-1,
                                                           fsm[3], state)])
    regex = re_union(regex_lst)
    #ask to print R terms
    to_print = input("Would you like to see the R terms? (enter 'yes') ")
    if to_print.lower() == "yes":
        for key, value in sorted(kleens_dict.items()):
            print("R",key,": ", value)
    return regex

#FIXme
################################################################################
################################################################################
# Test and verification functions
################################################################################

def verify_regex(regex, good_inputs, bad_inputs):
    # Uses the python re module to verify that all good inputs are accepted
    # by regex, and all bad_inputs are not accepted by regex
    # Function returns a 2-tuple of a boolean (passed), and a message
    # explaining the result.
    from re import fullmatch
    message = "regex '%s' passes testing " % regex
    for in_str in good_inputs:
        if not fullmatch(regex, in_str):
            message = "string '{}' from good input not matched".format(in_str)
            message += " by regex '%s'" % regex
            return False, message
    for in_str in bad_inputs:
        if fullmatch(regex, in_str):
            message = "string '{}' from bad input was matched".format(in_str)
            message += " by regex '%s'" % regex
            return False, message
    return True, message


def test_regexes():
    # Test out the verify_regex function using several sets of data
    print("Testing regex code:")
    regex_data_sets = []
    regex_data_sets.append(["abc*d",
                            ["abccccd", "abd", "abcd"],
                            ["abcdd", "acd", "abdd"]])
    regex_data_sets.append(["abc*dd",
                            ["abccccd", "abd", "abcd"],
                            ["abcdd", "acd", "abdd"]])
    regex_data_sets.append(["(a|b)c(d|e)",
                            ["acd", "bcd", "ace"],
                            ["bcb", "bca", "edc"]])
    regex_data_sets.append(["(a|b)c(d|e)",
                            ["acd", "bcd", "ace"],
                            ["bce",]])
    
    for rds in regex_data_sets:
      result = verify_regex(*rds)
      print("PASS:" if result[0] else "FAIL:", result[1])
    print("Done testing")


def verify_FSM(fsm, good_inputs, bad_inputs):
    # Steps each of the good inputs through the provided fsm, and verifies that
    # the good input strings all return true, and the bad input strings all
    # return false
    # Function returns a 2-tuple of a boolean (passed), and a message explaining
    # the result
    message = "fsm passes testing"
    for in_str in good_inputs:
        pass_test, msg = fsm_match_str(fsm, in_str)
        if not pass_test:
            message = "fsm fails testing: "
            message += "string '{}' from good input not matched".format(in_str)
            message += " ["+msg+"]"
            return False, message
    for in_str in bad_inputs:
        pass_test, msg = fsm_match_str(fsm, in_str)
        if pass_test:
            message = "re fails testing: "
            message += "string '{}' from bad input was matched".format(in_str)
            message += " ["+msg+"]"
            return False, message
    return True, message


def test_fsms():
    default_filename = "fsm_and_regex_data.txt"
    fsm_and_datasets = load_all_data_from_file(default_filename)
    for fsm, good_str, bad_str in fsm_and_datasets:
        print("Testing FSMs:\n", fsm)
        result = verify_FSM(fsm, good_str, bad_str)
        print("FSM - "+("PASS:" if result[0] else "FAIL:"), result[1])

        re = generate_re_from_fsm(fsm)
        result = verify_regex(re, good_str, bad_str)
        print("REGEX - "+("PASS:" if result[0] else "FAIL:"), result[1])


def test_regex_operations():
    # Run through a set of fundemental tests to see the results of applying
    # the re_union, re_concat, and re_star to some test data
    print("Test re_union(), re_concat(), and re_star():")
    test_set = (None, "", 'a', 'ab')
    for x in test_set:
        for y in test_set:
            print("re_union([{}, {}]):".format(repr(x), repr(y)),
                  repr(re_union([x, y])))
    for x in test_set:
        for y in test_set:
            print("re_concat({}, {}):".format(repr(x), repr(y)),
                  repr(re_concat(x, y)))
    for x in test_set:
        print("re_star({}):".format(repr(x)), repr(re_star(x)))

################################################################################
# Main area:
# - Call the runnable code here
################################################################################
test_regexes()
print("#"*80)
test_fsms()
print("#"*80)
test_regex_operations()
