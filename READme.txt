Description:
First we had to create a function that takes in an fsm and outputs a regular expression that can be used in order to determine if a given input string is accepted by the fsm.  This was done using iteration and kleen's algorithm. 
Part of this project was to create a regular language consiting of an FSM that we would be able to test in our program with the function mentioned earlier.  
Lastly we had to create functions for union, concatination, and kleen's closure which takes in data and conforms it to an output consiting of the union, concatination, or kleen's closure, then returning the data. 

My regular language:
States:
	s0, s1, s2
Alphabet:
	a, b
Initial state:
	s0
Accepting State:
	s1
non-Accepting States:
	s0, s2
Strings to be tested:
good:
	b
	bbbb
	bba	
	bbbaa
	aa
bad:
	bb
	a	
	ab	
	aba	
	bbaa

BUGS:
Program could simplify R terms better.

TODO:
make a better function to simplify R terms more and use the re_concat and re_star functions better.  